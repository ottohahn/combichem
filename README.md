# Creación de un software de química combinatoria

## Motivación

Los softwares que existen para química combinatoria son muy caros, o son de
código libre y abierto pero les falta funcionalidad, les falta versatilidad o
son difíciles de usar. Por lo que una utilidad sencilla que genere moléculas
basada en una o varias plantillas de SMILES, y que tenga una lista de diferentes
sustituyentes, puede convertirse en una herramienta útil para las
investigaciones en desarrollo de medicamentos.

## Objetivo

Crear una herramienta en línea de comandos que sea capaz de generar una
biblioteca combiantoria de moléculas que cumplan con ciertos requerimientos
(para hacer la búsqueda dirigida), entre dichos requerimeintos debe haber uno de
similaridad, y uno de novedad. Otros son de propiedades físicas y químicas como
el tamaño, peso molecular, Lipinski, drug-likeness y básicamente ya con eso.

El sistema debe poder funcionar desde línea de comandos leyendo un archivo de
plantillas, un archivo de indicadores y un archivo de sustituyentes.

Las moléculas generadas deben ser factibles químicamente por lo que aparte de
validar el SMILES se les generará una conformación 3D y se llevará a cabo una
optimización de geometría.

El sistema funcionará con un algoritmo genético combinando las fórmulas de
SMILES. Con las operaciones de mutación (inserción de un grupo, borrado de un
grupo, sustitución de un grupo), y combinación (2 moléculas se pueden combinar
en un punto de corte que sea válido para ambas, aqui es importante ver la fusión
de anillos).

## Tareas

- Revisar la bibliografía actual al respecto de la generación de bibliotecas
combinatorias y exhaustivas (Tal vez convenga mas generarlas como gráficas y
combinar gráficas).
- Crear la interfase de línea de comandos
- Definir cuales son posiciones donde se puede hacer una mutación con SMILES
- Crear las funciones de mutación
- Crear la función de adaptación
- Crear la función del cruce (1 respetando el scaffold, 2 sin respetarlo)
- Definir el formato de salida (CSV, pandas, SQLite)

## Definiciones funcionales

### Interfaz de línea de comandos

```
$ checombi -t template.smi -s susts.smi -f fitness.csv -p (csv, sqlite, pickle) 
-o outfile
```

Opciones:
- t --template: archivo con un SMILES por línea donde las posiciones a
  sustituir van definidas por asteriscos
- s --sustituents: archivo con un SMILES por línea donde van los grupos que se
  pueden sustituir en las plantillas
- f --fitness: descripción de la funcion de fitness en un archivo como separado
  por comas, va el descriptor y el peso, de una combinación lineal.
  f = w1*f1 + w2*f2 + ...
- p --outtype:  tipo de archivo de salida, puede ser csv (separado por pipes, un
 sqlite, o un archivo pickle de un dataframe de python
- o --outfile: nombre del archivo de salida, la extensión viene dada por la
  opción p

### Definición de las posiciones de mutación

Para esto vamos a crear un SMILES el cual va a tener en las posiciones en las que
queremos poner substituyentes un asterisco (lo cual es SMILES válido y permite
revisar que la plantilla sea válida).

### Definición de las funciones de mutación

Creo que debe haber dos tipos de mutaciones:

- Para generar moléculas nuevas a partir de la plantilla
- Para modificar una molécula ya existente

En el caso de moléculas nuevas, estoy pensando elegir un substituyente aleatorio
de la lista, de substituyentes provista por el usuario (que puede ser tan grande
como se quiera, si se quiere con todas las ramificaciones del mundo, etc...
y escoger uno al azar por cada posición pegarlos y medir. Hay que llevar la
cuenta de cuales se usaron para no repetir (demasiado) y tambien llevar la
cuenta para saber cuales no deben reaparecer si el fitness es muy malo.

Para modificar una molécula preexistente creo que en cada mutacieon se va a
elegir una posición aleatoria fuera de la plantila y se le va a poner un
substituyente, si por cuestiones de conectividad, no se pudiera, entonces se
elige otra posición. Aqui es importante notar que hay un subtipo de mutación que
es la ramificación donde no vamos a insertar un átomo, vamos a insertar un
grupo que sea una rama. Esto es importante porque con las ramas el SMILES
canónico cambia y entonces se puede volver un problema.

Existen otro tipo de mutaciones, las remociones de átomos, en estas se va a
elegir un átomo al azar, y se borra. Creo que las cosas entonces se deben hacer
sobre una representación de gráfica de las moléculas para evitarnos problemas de
conectividad, de ciclos y demás. En lugar de las representaciones de moléculas o
de las representaciones como strings y operar sobre el SMILES directamente.

Otra mutación creo que puede ser la ciclación, se eligen dos átomos a 5 o 6
lugares de distancia entre sí (o uno y se ve quienes son sus vecinos a 5 o 6 y 
se ciclan. 
 
### Función de adaptación 

Se me ocurrió una idea que creoq ue puede cambiar los alcances del proyecto, que
haya un modo exhaustivo donde solo le digas cuantas moléculas debe generar y que
garantice que sean diferentes entre sí. Con ese modo no hay función de
adaptación pero sirve para calcular los indicadores de la biblioteca.

En el modo de algoritmoo genético, entonces si se debe definir una fucnion de
adaptación que creo que lo mas sencillo es que sea de dos tipos posibles:
- combinación lineal de indicadores.
- funcion con ifs de indicadores

Hay que ver como pasar los diferentes tipos de coeficientes para cada función.
De manera que se puedan leer fácilmente.

### Función de cruce

Esta función de cruce entre dos moléculas solo se puede dar en el modo de
algoritmo genético, debe tener un parámetro para respetar el scaffold o no, y si
lo respeta, entonces combina los diferentes sustituyentes de a y b. Para cada
posición disponible elige un sustituyente de A o de B siempre eligiendo mas o
menos igual o al menos 1 de cada una. (si solo hay una posición para sustituir,
no debe llevar a cabo el cruce).

en la versión en la que no respeta el scaffold, se debe trabajar sobre las
gráficas moleculares, eligiendo posiciones de corte (si la posición cae en un
ciclo, ¿se añade como sustituyente? ¿o se rompe el ciclo?)

Para lo demás, creo que es muy fácil unir los pedazos y verificar las valencias,
si ese pedazo elegido da una valencia inválida se elige uno nuevo (hasta 10
veces oo algo así)

### Definir el formato de salida

Para definir el formato de salida, creo que uno debe tener una estructura
subyacente en algún tipo de formato permanente (como SQLite) y luego dejarlo
o exportar a un dataframe, o a un csv.

Aparte con SQLite se pueden generar tablas intermedias y es permanente el guardado.


