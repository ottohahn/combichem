#!/usr/bin/env python
"""
$ checombi -t template.smi -s susts.smi -f fitness.csv -p (csv, sqlite, pickle)
-o outfile
"""
import itertools
import argparse

def read_template_file(inpfile):
    """
    Read the template into a list
    """
    amol = ''
    tempfile = open(inpfile, 'r')
    amol = tempfile.read().replace('\n','')
    tempfile.close()
    return amol

def read_groups_file(gfile):
    """
    read the groups into a list
    """
    groups = []
    gpfile = open(gfile, 'r')
    groups = [g.strip() for g in gpfile.readlines()]
    gpfile.close()
    return groups

def generate_mols(template, groups, db_name):
    """
    Function to generate molecules in an exhaustive manner from the template and
    the groups. It calculates the number of possible molecules and iterates over
    it.
    It counts the groups, and the available positions and calculates the number
    n^r of molecules to generate
    """
    
    
    # Count the number of available positions
    n = template.count('*')
    res = list(itertools.product(groups, repeat=n))
    for sl in res:
        newstr = template
        for group in sl:
            newstr = newstr.replace('*', group, 1)
        db_name.write(newstr+'\n')
    db_name.close()
    
def main():
    """
    Main function, passes args from command line, calls the functionality
    """
    parser = argparse.ArgumentParser(description="Combinatorial library generation")
    parser.add_argument("-t", "--template", help="""smi file with the template or 
                    templates for the library""")
    parser.add_argument("-s", "--sustituents", help="""smi file with a list of 
                    functional groups to sustitute in the template""")
    parser.add_argument("-o", "--outfile", help="""name of the output file""")
    args = parser.parse_args()
    # Main actions
    db_name = open(args.outfile, 'w+')
    template = read_template_file(args.template)
    groups = read_groups_file(args.sustituents)
    generate_mols(template, groups, db_name)
    return 0

if __name__ == '__main__':
    main()
